import ballerina/grpc;
import ballerina/log;
import ballerina/io;
import ballerina/crypto;
import ballerina/file;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {
    descriptor: ROOT_DESCRIPTOR,
    descMap: getDescriptorMap()
}
service songStorage on ep {

    resource function insertNewRecord(grpc:Caller caller, Record value) {
        // Implementation goes here.

        // You should return a savedResponse
          // Implementation goes here.
          //getting the entire record
         string record_value = value.toString();
          //turn iyt to bytes
         byte[] recArray = record_value.toBytes();
         //encrypt the  bytes to SHA256
         byte[] reckey= crypto:hashSha256(recArray);
         var  readResult="";
         //turn the record into json by mapping it
        map<json>|error Myrecord = <map<json>>(value);
       // You should return a savedResponse
        //check if the above convertion- is error
        if (Myrecord is error) {
            io:println("Error");
        } else {
            
            //inserting record in json object
         //   Myrecord["RecordKey"] = reckey.toBase16();
            string filePath = "./files/" + reckey.toBase16()+  ".json";

           // write to json file
              var writeResults = write(Myrecord, filePath);//write to a json file 
                        
                 readResult =  read(filePath).toString();
        }
           grpc:Error? result = caller->send({key: reckey.toBase16(), 'version: 1});
           
          // grpc:Error? result = caller->send(readResult);
            if (result is grpc:Error) {
                result = caller->sendError(404, "Page Not Found");
            }
           result = caller->complete();
    }
    resource function updateExistingRecord(grpc:Caller caller, updateRecord value) {
        // Implementation goes here.
  
  // This will Allows us to read key from the file directory
  // and compare 

         string filePath = "./files/"+value["key"].toString();
         

     boolean ExistingFile = file:exists(<@untainted>filePath);
if (ExistingFile.toString()== "true"){
    value["version"] =value["version"]+1;
    var writeResults = write(value,<@untainted>filePath);
    //var writeAgain= write(,<@untainted>filePath);


 } else {io:println("Error record does not exist");
 // if  the key matches then the
 }
        // You should return a Confirm
          
    }
 
    resource function readRecordWithKey(grpc:Caller caller, readRecordKey value) {
    
         string filePath = "./files/"+value["key"].toString();
           boolean ExistingFile = file:exists(<@untainted>filePath);
         if (ExistingFile.toString() == "true"){
             var readRecord = read(Record.toString());


         }else{io:println("Key is wrong");} 
        

        // You should return a Record
    }
    resource function readRecordWithKeyVersion(grpc:Caller caller, savedResponse value) {
        // Implementation goes here.
 string filePath = "./files/"+value["version"].toString();
  boolean ExistingFile = file:exists(<@untainted>filePath);
  string Version = value.toString();
   if (ExistingFile.toString()== "true"){
       var readVersion = read(Record.toString());
   }

        // You should return a Record
    }
    resource function readRecordCriterion(grpc:Caller caller, readWithCriterion value) {
        // Implementation goes here.

        // You should return a AllRecords
    }
}

function closeRc(io:ReadableCharacterChannel rc) {
    var result = rc.close();
    if (result is error) {
        log:printError("Error occurred while closing character stream",
                        err = result);
    }
}

function closeWc(io:WritableCharacterChannel wc) {
    var result = wc.close();
    if (result is error) {
        log:printError("Error occurred while closing character stream",
                        err = result);
    }
}
function write(json content, string path) returns @tainted error? {

    io:WritableByteChannel wbc = check io:openWritableFile(path);

    io:WritableCharacterChannel wch = new (wbc, "UTF8");
    var result = wch.writeJson(content);
    closeWc(wch);
    return result;
}

function read(string path) returns @tainted json|error {

    io:ReadableByteChannel rbc = check io:openReadableFile(path);

    io:ReadableCharacterChannel rch = new (rbc, "UTF8");
    var result = rch.readJson();
    closeRc(rch);
    return result;
}

public type AllRecords record {|
    Record[] records = [];
    
|};

public type readWithCriterion record {|
    string songtitle = "";
    string artistname = "";
    string bandName = "";
    
|};

public type savedResponse record {|
    string key = "";
    int 'version = 0;
    
|};

public type readRecordKey record {|
    string key = "";
    
|};

public type Confirm record {|
    string confirm = "";
    
|};

public type Record record {|
    string name = "";
    string date = "";
    string band = "";
    Song[] songs = [];
    Artist[] artist = [];
    
|};

public type Song record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};


public type Artist record {|
    string name = "";
    Member? member = ();
    
|};

public type Member "YES"|"NO";
public const Member MEMBER_YES = "YES";
const Member MEMBER_NO = "NO";


public type updateRecord record {|
    string key = "";
    int 'version = 0;
    Record? 'record = ();
    
|};



const string ROOT_DESCRIPTOR = "0A0A63616C692E70726F746F22C8020A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E41727469737452066172746973741A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F1001223B0A0D7361766564526573706F6E736512100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E225B0A0C7570646174655265636F726412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E121F0A067265636F726418032001280B32072E5265636F726452067265636F726422230A07436F6E6669726D12180A07636F6E6669726D1801200128095207636F6E6669726D22210A0D726561645265636F72644B657912100A036B657918012001280952036B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F7264733285020A0B736F6E6753746F72616765122A0A0F696E736572744E65775265636F726412072E5265636F72641A0E2E7361766564526573706F6E7365122F0A147570646174654578697374696E675265636F7264120D2E7570646174655265636F72641A082E436F6E6669726D122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F726412330A18726561645265636F7264576974684B657956657273696F6E120E2E7361766564526573706F6E73651A072E5265636F726412360A13726561645265636F7264437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33";
isolated function getDescriptorMap() returns map<string> {
    return {
        "cali.proto":"0A0A63616C692E70726F746F22C8020A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E41727469737452066172746973741A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F1001223B0A0D7361766564526573706F6E736512100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E225B0A0C7570646174655265636F726412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E121F0A067265636F726418032001280B32072E5265636F726452067265636F726422230A07436F6E6669726D12180A07636F6E6669726D1801200128095207636F6E6669726D22210A0D726561645265636F72644B657912100A036B657918012001280952036B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F7264733285020A0B736F6E6753746F72616765122A0A0F696E736572744E65775265636F726412072E5265636F72641A0E2E7361766564526573706F6E7365122F0A147570646174654578697374696E675265636F7264120D2E7570646174655265636F72641A082E436F6E6669726D122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F726412330A18726561645265636F7264576974684B657956657273696F6E120E2E7361766564526573706F6E73651A072E5265636F726412360A13726561645265636F7264437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33"
        
    };
}

