import ballerina/grpc;

public client class songStorageBlockingClient {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public isolated function init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public isolated remote function insertRecord(Record req, grpc:Headers? headers = ()) returns ([savedResponse, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("songStorage/insertRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<savedResponse>result, resHeaders];
        
    }

    public isolated remote function updatingExistingRecord(updateRecord req, grpc:Headers? headers = ()) returns ([Confirmation, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("songStorage/updatingExistingRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Confirmation>result, resHeaders];
        
    }

    public isolated remote function readRecordWithKey(readRecordKey req, grpc:Headers? headers = ()) returns ([Record, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("songStorage/readRecordWithKey", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Record>result, resHeaders];
        
    }

    public isolated remote function readRecordWithKeyVer(savedResponse req, grpc:Headers? headers = ()) returns ([Record, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("songStorage/readRecordWithKeyVer", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<Record>result, resHeaders];
        
    }

    public isolated remote function readRecordWithCriterion(readWithCriterion req, grpc:Headers? headers = ()) returns ([AllRecords, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("songStorage/readRecordWithCriterion", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<AllRecords>result, resHeaders];
        
    }

}

public client class songStorageClient {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public isolated function init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public isolated remote function insertRecord(Record req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("songStorage/insertRecord", req, msgListener, headers);
    }

    public isolated remote function updatingExistingRecord(updateRecord req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("songStorage/updatingExistingRecord", req, msgListener, headers);
    }

    public isolated remote function readRecordWithKey(readRecordKey req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("songStorage/readRecordWithKey", req, msgListener, headers);
    }

    public isolated remote function readRecordWithKeyVer(savedResponse req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("songStorage/readRecordWithKeyVer", req, msgListener, headers);
    }

    public isolated remote function readRecordWithCriterion(readWithCriterion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("songStorage/readRecordWithCriterion", req, msgListener, headers);
    }

}

public type AllRecords record {|
    Record[] records = [];
    
|};


public type readWithCriterion record {|
    string songtitle = "";
    string artistname = "";
    string bandName = "";
    
|};


public type savedResponse record {|
    string key = "";
    int 'version = 0;
    
|};


public type readRecordKey record {|
    string key = "";
    
|};


public type Record record {|
    string name = "";
    string date = "";
    string band = "";
    Song[] songs = [];
    Artist[] artist = [];
    
|};

public type Song record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};


public type Artist record {|
    string name = "";
    Member? member = ();
    
|};

public type Member "YES"|"NO";
public const Member MEMBER_YES = "YES";
const Member MEMBER_NO = "NO";



public type Confirmation record {|
    string confirm = "";
    
|};


public type updateRecord record {|
    string key = "";
    int 'version = 0;
    Record? 'record = ();
    
|};



const string ROOT_DESCRIPTOR = "0A0A63616C692E70726F746F22C8020A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E41727469737452066172746973741A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F1001223B0A0D7361766564526573706F6E736512100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E225B0A0C7570646174655265636F726412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E121F0A067265636F726418032001280B32072E5265636F726452067265636F726422280A0C436F6E6669726D6174696F6E12180A07636F6E6669726D1801200128095207636F6E6669726D22210A0D726561645265636F72644B657912100A036B657918012001280952036B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F7264733289020A0B736F6E6753746F7261676512270A0C696E736572745265636F726412072E5265636F72641A0E2E7361766564526573706F6E736512360A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A0D2E436F6E6669726D6174696F6E122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F7264122F0A14726561645265636F7264576974684B6579566572120E2E7361766564526573706F6E73651A072E5265636F7264123A0A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33";
isolated function getDescriptorMap() returns map<string> {
    return {
        "cali.proto":"0A0A63616C692E70726F746F22C8020A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E41727469737452066172746973741A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F1001223B0A0D7361766564526573706F6E736512100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E225B0A0C7570646174655265636F726412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E121F0A067265636F726418032001280B32072E5265636F726452067265636F726422280A0C436F6E6669726D6174696F6E12180A07636F6E6669726D1801200128095207636F6E6669726D22210A0D726561645265636F72644B657912100A036B657918012001280952036B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F7264733289020A0B736F6E6753746F7261676512270A0C696E736572745265636F726412072E5265636F72641A0E2E7361766564526573706F6E736512360A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A0D2E436F6E6669726D6174696F6E122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F7264122F0A14726561645265636F7264576974684B6579566572120E2E7361766564526573706F6E73651A072E5265636F7264123A0A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33"
        
    };
}

