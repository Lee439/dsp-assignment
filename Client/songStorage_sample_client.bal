import ballerina/grpc;
import ballerina/io;
import ballerina/lang.'int;

public function main (string... args) {

    songStorageBlockingClient blockingEp = new("http://localhost:9090"); 

                            io:println(" --------------Cali Storage System-------------------");
                              io:println(" Select option by entering in  a number");
                              io:println("  1. Enter Record  ");
                              io:println("  2. Update Record ");
                              io:println("  3. Read Record with Key ");
                              io:println("  4. Read  Record with Version");
                              io:println("");
       string choice =io:readln("Select your choice :");

          if (choice == "1"){
            // write into a record 
           string name = io:readln("Enter Name :");
           string date  = io:readln("Enter Date  :");
           string band_name  = io:readln("Enter Band Name  :");
           string songs  =  io:readln("Enter Number Of Songs To Be Entered :");
           // get number os songs and entering into array 
           int|error song_max_number_to_be_input = 'int:fromString(songs);
    
    //using proto generated array to store songs 

            Song[] array_of_songs = [];
                if (song_max_number_to_be_input is int) {
            
                    foreach var j in 1...song_max_number_to_be_input {
                        string title = io:readln( "Enter title  :");
                        string genre  = io:readln("Enter genre :");
                        string platform  = io:readln("Enter platform :"); 
                        //PUSH THE RESULTS IN THE ARRAY
                        array_of_songs.push({"title":title,"genre":genre,"platform":platform});
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }
             //GETTING USER INPUT FOR MAXIMUM NUMBER OF ARTIST
            string artists  =  io:readln("Enter Number Of artist to be entered  :");
           int|error artist_max_number_to_be_input = 'int:fromString(artists);
             //USING THE Artist[] ARRAY ALREADY CREATED IN THE PROTO TO STORE Artist DETAILS
            Artist[] array_of_artist = [];
                if (artist_max_number_to_be_input is int) {
                   //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF Artist USER WANTS TO INPUT     
                    foreach var j in 1...artist_max_number_to_be_input {
                        string Artist_name= io:readln( "Enter name  :");
                        string member  = io:readln("Enter member YES|NO :");
                        //PUSH THE RESULTS IN THE ARRAY
                        if (member==="yes"){
                             array_of_artist.push({"name":Artist_name,"member":MEMBER_YES});
                        }else{
                              array_of_artist.push({"name":Artist_name,"member":MEMBER_NO});
                        }
                       
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }

           var result = blockingEp->insertNewRecord({  "name":name,"date":date,
                                        "band":band_name,"songs":array_of_songs,
                                            "artist":array_of_artist
                                           
                                     });
                //return response 
                    if(result is grpc:Error){
                                io:println("ERROR");
                    }else{
                        io:println(result);
                    }       
       } else if (choice =="2"){
string name = io:readln("Enter Name :");
           string date  = io:readln("Enter Date  :");
           string band_name  = io:readln("Enter Band Name  :");
           string songs  =  io:readln("Enter Number Of Songs To Be Entered :");
           // get number os songs and entering into array 
           int|error song_max_number_to_be_input = 'int:fromString(songs);
    
    //using proto generated array to store songs 

            Song[] array_of_songs = [];
                if (song_max_number_to_be_input is int) {
            
                    foreach var j in 1...song_max_number_to_be_input {
                        string title = io:readln( "Enter title  :");
                        string genre  = io:readln("Enter genre :");
                        string platform  = io:readln("Enter platform :"); 
                        //PUSH THE RESULTS IN THE ARRAY
                        array_of_songs.push({"title":title,"genre":genre,"platform":platform});
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }
             //GETTING USER INPUT FOR MAXIMUM NUMBER OF ARTIST
            string artists  =  io:readln("Enter Number Of artist to be entered  :");
           int|error artist_max_number_to_be_input = 'int:fromString(artists);
             //USING THE Artist[] ARRAY ALREADY CREATED IN THE PROTO TO STORE Artist DETAILS
            Artist[] array_of_artist = [];
                if (artist_max_number_to_be_input is int) {
                   //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF Artist USER WANTS TO INPUT     
                    foreach var j in 1...artist_max_number_to_be_input {
                        string Artist_name= io:readln( "Enter name  :");
                        string member  = io:readln("Enter member YES|NO :");
                        //PUSH THE RESULTS IN THE ARRAY
                        if (member==="yes"){
                             array_of_artist.push({"name":Artist_name,"member":MEMBER_YES});
                        }else{
                              array_of_artist.push({"name":Artist_name,"member":MEMBER_NO});
                        }
              string myKey = io:readln("enter key");
              string  TheVersion = io:readln("version");
    
   
   int|error RecordVersion = 'int:fromString(TheVersion);
  int [] vers=[]; 
     if (RecordVersion is int ){
            string  myVesion = io:readln("version");
           int aa = RecordVersion;// type casting user string input to interger 
             var update = blockingEp -> updateExistingRecord({"key":myKey,"version":aa,"record":{  "name":name,"date":date,
                                        "band":band_name,"songs":array_of_songs,
                                            "artist":array_of_artist}});}

   else {io:println("not version");
   } 



readRecordKey read = {key:"92f4c070ff094a32a38132c8c3bc01ba"};
//client sends key to the server 

       }
     // Sends the record, key and version                    
Record recthing = { "name":"Lemuel","date":"21/12/2020",
                                         "band":"Big Time rush","songs":[
                                           {"title":" One Time","genre":"pop","platform":"apple Music"}
                                             ,{"title":"Dynamite","genre":"pop","platform":"Itunes"},
                                        {"title":"Steal my girl","genre":"pop","platform":"youtube"},
                                             {"title":"Believer","genre":"Rock","platform":"Radio"}//This is the record 
                                             ],
                                             "artist":[
                                                 {"name":"Leeroy","member":"YES"},{"name":"Junior","member":"NO"},
                                                 {"name":"Tafadzwa","member":"NO"},
                                                 {"name":"Jonny","member":"YES"},
                                                 {"name":"Lele","member":"YES"}
                                           ]};

var update = blockingEp -> updateExistingRecord({"key":"92f4c070ff094a32a38132c8c3bc01ba","version":1,"record":recthing});

if (update is grpc:Error){io:println(recthing);
}else {io:println(update);}


readRecordKey read = {key:"92f4c070ff094a32a38132c8c3bc01ba"};
//client sends key to the server 
var readRec = blockingEp ->readRecordWithKey (read);

  if ( update is grpc:Error){
        io:println(readRec);
                 }

  var readVersion = blockingEp -> readRecordWithKeyVersion({"version":1});  

}